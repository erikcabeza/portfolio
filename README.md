# Erik's Portfolio

## Description

Welcome to my online portfolio! My name is Erik. I currently work as data scientist /data engineer in Axes In Motion, a mobile video game publisher. I've worked as a backend developer too and I've got experience volunteering as data analyst in a soccer club.

My two areas of interest are videogames and sports. 

In the context of videogames, I like narrative design, game design and, of course, data analysis. 

In the context of sports, I'm interested in the application of data to it, specially in the contexts of performance and tactical analysis.

The structure of my portfolio is the following one:

1 - videogames_projects: Here you'll find all my projects related to the videogames world. You'll find samples of game design, narrative design and data analysis. 

2 - sports_projects: Here you'll find all my projects related to sports. 


## Contact Info

Here's the link to my LinkedIn page: https://es.linkedin.com/in/erik-cabeza/es
