{
  "nbformat": 4,
  "nbformat_minor": 0,
  "metadata": {
    "colab": {
      "provenance": []
    },
    "kernelspec": {
      "name": "python3",
      "display_name": "Python 3"
    },
    "language_info": {
      "name": "python"
    }
  },
  "cells": [
    {
      "cell_type": "markdown",
      "source": [
        "# A/B testing for mobile videogames: the guide from scratch.\n",
        "\n",
        "Author: Erik Cabeza\n",
        "\n",
        "In this **notebook** I'll explain how to apply A/B testing in the **mobile gaming industry**. You'll learn about all the **basic concepts** and what do you need to do in order to perform a **successful A/B test**, from **designing** it to **analyzing** it.\n",
        "\n",
        "\n"
      ],
      "metadata": {
        "id": "LR5lk-DuuOQX"
      }
    },
    {
      "cell_type": "markdown",
      "source": [
        "# 1. What is A/B Testing?\n",
        "\n",
        "A/B testing involves conducting an experiment with two versions: the baseline (**A**, the **standard**) and the **variant** (**B**) of the same *app*, *website*, etc. The purpose of the test is to determine whether there is a **significant difference** between the two versions, which is not due to chance.\n",
        "\n",
        "These two versions differ in a **concrete element**. For example, in the **context** of **mobile game development**, **variant** **B** could have different **menu colors**.\n",
        "\n",
        "If you compare more than two versions, for example, three, then you are performing an **A/B/C test**.\n",
        "\n",
        "In the **context** of **mobile game development**, it's really common to launch **A/B tests** using the [Firebase A/B Testing Console](https://firebase.google.com/docs/ab-testing?hl=en). *Firebase* is a **platform** for *mobile app development*, and its **A/B Testing** feature allows you to launch **A/B tests** easily using [Remote Config](https://firebase.google.com/docs/remote-config?hl=en).\n",
        "\n"
      ],
      "metadata": {
        "id": "fxkqSuZE1jD1"
      }
    },
    {
      "cell_type": "markdown",
      "source": [
        "# 2. When I'm applying A/B Testing?\n",
        "\n",
        "You are the owner of a popular **terror mobile game** called \"*Knock, Knock, Survive*.\" This game consists of several **levels**. To surpass each **level**, you'll have to open the \"**right**\" door. If you don't, a **nasty monster** will come out from the **wrong** door, and the game will end.\n",
        "\n",
        "Each **level** will give you a **clue** for guessing which is the **correct** door to open.\n",
        "\n",
        "After completing two levels, **an interstitial ad** will pop out.\n",
        "\n",
        "During the last weeks, your **team** has been working on a new **version** of the game, in which now you'll have a **limited** amount of time for choosing a door. Before, you could spend all the time you wanted thinking about your choice. Now, you'll have to act quickly.\n",
        "\n",
        "Your **team** is going to deploy this new **version** of the **app** with a **20% rollout** on **Google Play**, to test if it's well-received by your **players**. For doing this, you'll have to measure the **KPIs** of this **version** and compare them with the ones of the **old version**, which will still be deployed at **80%**.\n",
        "\n",
        "What does this example have to do with **A/B testing**, you may be thinking. Well, even if you are not going to create an **A/B test** using a **tool** like *Firebase A/B Testing Console*, in the end, you'll have to follow the same **procedure** as you would when performing an **A/B test** because you are comparing two **versions** of the same **product**.\n",
        "\n",
        "In conclusion, in **mobile game development**, you'll often find yourself both conducting traditional **A/B testing** and comparing two **versions** of an **app**. For both scenarios, you can apply the same **system** for analyzing the **data**.\n",
        "\n"
      ],
      "metadata": {
        "id": "j2wJylHIvYY6"
      }
    },
    {
      "cell_type": "markdown",
      "source": [
        "# 3. Should I use A/B Testing or multivariate testing?\n",
        "\n",
        "When deciding between launching a **standard A/B test** or **comparing** two **versions** of an **app**:\n",
        "\n",
        "Opt for an **A/B test** if you're interested in assessing the **impact** of altering a **single feature** or evaluating the **combined effect** of **multiple changes**.\n",
        "Choose **multivariate testing** if the **variant** or **latest app version** introduces **alterations** to **multiple features**, and you aim to measure the **individual impact** of each **change**.\n",
        "\n",
        "In this **notebook**, we're going to focus on **A/B testing**.\n",
        "\n",
        "\n"
      ],
      "metadata": {
        "id": "eQLM_VXP31Hr"
      }
    },
    {
      "cell_type": "markdown",
      "source": [
        "# 4. How can I design an A/B test using the Firebase A/B Testing Console?\n",
        "\n",
        "For this **purpose**, you will need **Firebase Remote Config**, as mentioned before.\n",
        "\n",
        "This **tool** enables you to modify the **behavior** and **appearance** of your **app** without requiring users to download an **app update**.\n",
        "\n",
        "Since this **guide** focuses on the **analytical aspect** of **A/B Testing**, I'll leave the **instructions** for setting up **Remote Config** for your **app** here: [Firebase Remote Config Get Started](https://firebase.google.com/docs/remote-config/get-started?platform=android)\n",
        "\n",
        "Additionally, here's the **link** to the **guide** on how to launch an **A/B test** in the **Firebase console**: [Firebase A/B Testing Guide](https://firebase.google.com/docs/ab-testing/abtest-config)\n",
        "\n",
        "**Before**, the **A/B testing tool** of **Firebase** used **Bayesian inference**, but **since 2023-10-23** it's using **Frequentist inference**.\n",
        "\n",
        "\n",
        "\n"
      ],
      "metadata": {
        "id": "KBI4GEJI423j"
      }
    },
    {
      "cell_type": "markdown",
      "source": [
        "# 5. How much time should my experiment run? Or, if I'm comparing two app versions deployed simultaneously, how long should I wait before analyzing the KPIs?\n",
        "\n",
        "For both **cases**, you will need to run the **experiment** for enough **time** to ensure that you have a **minimal quantity** of **users** for a **minimum 80% statistical power** for your **hypothesis testing**.\n",
        "\n",
        "Having a minimum **statistical power** of 80% is often considered a standard in hypothesis testing because it ensures a reasonable chance of detecting a **true effect** if it exists, meaning that this effect is not due to chance.\n",
        "\n",
        "Ultimately, you have to consider the **volume** of your **daily active users**. If you have **few daily active users**, then you'll need to wait longer.\n",
        "\n",
        "As a **standard**, a **minimum** of **two weeks** and no more than **one month** is usually required to ensure that you have enough **users** for analyzing the **data**."
      ],
      "metadata": {
        "id": "I6HhEyOS6SBy"
      }
    },
    {
      "cell_type": "markdown",
      "source": [
        "# 6. How should I interpret the results returned by the Firebase A/B Testing console?\n",
        "\n",
        "The **Firebase console** provides the following information for both the **baseline** and the **variant(s)** for each selected metric when creating the A/B Test (such as revenue, retention at day 1, etc.):\n",
        "\n",
        "- **Conversion rate**: if the metric is a rate (e.g., retention at day 1), or the **mean value** if the metric is numerical.\n",
        "- **Standard Deviation**: for numerical variables.\n",
        "- **Percentage change** from baseline to variant.\n",
        "- **Confidence interval**: an interval indicating the \"true\" value of the tracked metric with 95% confidence.\n",
        "- **p-value**: indicating the statistical significance of observed differences.\n",
        "\n",
        "Interpreting the **confidence interval** and **p-value** is crucial for determining if the variant is better. Firebase A/B Testing employs **Frequentist inference**, specifically a **one-tailed hypothesis test**, assuming the **variant** is **superior** to the baseline.\n",
        "\n",
        "Understanding this is vital, as the interpretation of the **p-value** varies based on the hypothesis test type:\n",
        "\n",
        "- A **p-value** of 1 doesn't signify a lack of statistical significance; instead, it indicates the variant significantly underperformed compared to the baseline.\n",
        "- A **p-value** equal to or below 0.05 suggests the variant significantly outperforms the baseline in that specific KPI.\n",
        "\n",
        "Regarding the **confidence interval**:\n",
        "- If the **interval** **includes** **0**, a **statistically** **significant** difference between the variant and baseline is **not** observed.\n",
        "\n",
        "\n",
        "**Just an additional quick note:** If you have not just one variant, but several, you should apply **Bonferroni correction**, which divides the desired **significance level** (usually 0.05, the one used by Firebase by default) by the number of comparisons. For instance, if you have one **baseline** and two **variants**, you would compare each **variant** against the **baseline**. In this case, since you have two comparisons, you'd use a **significance level** of 0.05 / 2 = 0.025 for each comparison.\n",
        "\n",
        "This adjustment helps **reduce** the likelihood of incorrectly rejecting the **null hypothesis** due to multiple comparisons.\n",
        "\n",
        "\n",
        "\n",
        "\n",
        "\n",
        "\n",
        "\n",
        "\n"
      ],
      "metadata": {
        "id": "PKgxErU0QBhA"
      }
    },
    {
      "cell_type": "markdown",
      "source": [
        "# 6. The main core of this article. How should I analyze the A/B testing data or the data for comparing two versions of the same app?\n",
        "\n",
        "In both cases, you'll need to utilize **hypothesis testing**.\n",
        "\n",
        "**Hypothesis testing** is a statistical method used to validate if changes in your variant or app version are significant and not merely due to chance. If you assume that the variant or another app version is better than the baseline or standard app version, you would need to perform a **one-tailed hypothesis test**.\n",
        "\n",
        "\n",
        "While the insights provided by the Firebase console are valuable, it's advisable to perform your own analysis using the [data exported from Firebase Google Analytics to BigQuery](https://firebase.google.com/docs/analytics#integrations_with_other_services). This allows for a more complex analysis, enabling you to filter users from non-legitimate stores, examine the impact of variant changes on specific cohorts (like the New Users cohort), etc.\n",
        "\n",
        "Remember the scenario introduced at the beginning of this guide:\n",
        "\n",
        "\"Your **team** is going to deploy a new **version** of the **app** with a **20% rollout** on **Google Play**, to test if it's well-received by your **players**. For doing this, you'll have to measure the **KPIs** of this **version** and compare them with the ones of the **old version**, which will still be deployed at **80%**.\"\n",
        "\n",
        "You'll conduct a **one-sided hypothesis test** assuming that the new app version will have  better KPIs: average number of sessions, average time spent per user, and average ad revenue per user. Your objective is to verify if indeed this new app version is better.\n",
        "\n",
        "Suppose you've processed the data exported from Firebase Google Analytics to BigQuery, resulting in a dataframe with the following columns:\n",
        "\n",
        "- **date**: Date when the information was recorded.\n",
        "- **user_id**: Unique identifier of the user.\n",
        "- **app_version**: Version of the app.\n",
        "- **store**: Store used by the user to install the app.\n",
        "- **date_first_session**: Date of the user's first session.\n",
        "- **sessions_number**: Total number of sessions by the user on that date.\n",
        "- **minutes_spent**: Total minutes spent by the user in the app.\n",
        "- **produced_ad_revenue**: Total ad revenue generated by the user on that date.\n",
        "- **produced_iap_revenue**: Total in-app purchase revenue generated by the user on that date.\n",
        "\n",
        "\n",
        "With the following code, we're going to generate a mock dataset with these columns.\n"
      ],
      "metadata": {
        "id": "S1M5h5JvAaVm"
      }
    },
    {
      "cell_type": "code",
      "execution_count": 88,
      "metadata": {
        "id": "E3QdyEvgpW3-"
      },
      "outputs": [],
      "source": [
        "#Import libraries\n",
        "import pandas as pd\n",
        "import random\n",
        "from datetime import datetime, timedelta\n",
        "\n",
        "# Generate mock data for January for 5000 distinct users\n",
        "january_days = 31  # January has 31 days\n",
        "num_users = 5000\n",
        "\n",
        "data = {\n",
        "    'date': [],\n",
        "    'user_id': [],\n",
        "    'date_first_session': [],\n",
        "    'app_version': [],\n",
        "    'store': [],\n",
        "    'sessions_number': [],\n",
        "    'minutes_spent_in_the_app': [],\n",
        "    'produced_ad_revenue': [],\n",
        "    'produced_iap_revenue': []\n",
        "}\n",
        "\n",
        "for user_id in range(1, num_users + 1):\n",
        "    for day in range(1, january_days + 1):\n",
        "        data['date'].append((datetime(2024, 1, day)).strftime('%Y-%m-%d'))\n",
        "        data['user_id'].append(user_id)\n",
        "        data['date_first_session'].append((datetime(2024, 1, day)).strftime('%Y-%m-%d'))\n",
        "        data['app_version'].append('1.2.1' if user_id % 2 == 0 else '1.2.2')\n",
        "        data['store'].append('Google Play' if user_id % 3 != 0 else 'Not Google Play')\n",
        "        data['sessions_number'].append(random.randint(1, 8))\n",
        "        data['minutes_spent_in_the_app'].append(random.uniform(1, 24))\n",
        "        data['produced_ad_revenue'].append(random.uniform(0.001, 0.10))\n",
        "        data['produced_iap_revenue'].append(random.uniform(1, 10))\n",
        "\n",
        "# Create DataFrame\n",
        "df = pd.DataFrame(data)\n"
      ]
    },
    {
      "cell_type": "code",
      "source": [
        "#Let's check if everything went smoothly\n",
        "print(df.info())\n",
        "#And let's have a peak at the dataset\n",
        "print(df.head(3))"
      ],
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "2B_gMsJ3sJ34",
        "outputId": "28169956-608e-4839-f5ea-6080d0bf17b1"
      },
      "execution_count": 89,
      "outputs": [
        {
          "output_type": "stream",
          "name": "stdout",
          "text": [
            "<class 'pandas.core.frame.DataFrame'>\n",
            "RangeIndex: 155000 entries, 0 to 154999\n",
            "Data columns (total 9 columns):\n",
            " #   Column                    Non-Null Count   Dtype  \n",
            "---  ------                    --------------   -----  \n",
            " 0   date                      155000 non-null  object \n",
            " 1   user_id                   155000 non-null  int64  \n",
            " 2   date_first_session        155000 non-null  object \n",
            " 3   app_version               155000 non-null  object \n",
            " 4   store                     155000 non-null  object \n",
            " 5   sessions_number           155000 non-null  int64  \n",
            " 6   minutes_spent_in_the_app  155000 non-null  float64\n",
            " 7   produced_ad_revenue       155000 non-null  float64\n",
            " 8   produced_iap_revenue      155000 non-null  float64\n",
            "dtypes: float64(3), int64(2), object(4)\n",
            "memory usage: 10.6+ MB\n",
            "None\n",
            "         date  user_id date_first_session app_version        store  \\\n",
            "0  2024-01-01        1         2024-01-01       1.2.2  Google Play   \n",
            "1  2024-01-02        1         2024-01-02       1.2.2  Google Play   \n",
            "2  2024-01-03        1         2024-01-03       1.2.2  Google Play   \n",
            "\n",
            "   sessions_number  minutes_spent_in_the_app  produced_ad_revenue  \\\n",
            "0                8                  3.291192             0.047435   \n",
            "1                1                 11.713555             0.066321   \n",
            "2                3                  4.252097             0.050188   \n",
            "\n",
            "   produced_iap_revenue  \n",
            "0              9.465132  \n",
            "1              3.539536  \n",
            "2              4.036718  \n"
          ]
        }
      ]
    },
    {
      "cell_type": "markdown",
      "source": [
        "**It seems like everything is in order. Let's remember:** we wanted to check if our **new version** that is deployed at **20% rollout** on **Google Play** it's working better than the one deployed at **80% rollout** in terms of **generic KPIs**: **average number of sessions per user**, **average ad revenue per user** and so **on**.\n",
        "\n",
        "- The **new version** has the name **1.2.2**.\n",
        "- The **old version** has the name **1.2.1**.\n",
        "\n",
        "**Let's create a dataset** for each version.\n",
        "\n",
        "\n"
      ],
      "metadata": {
        "id": "EB12-TXal-Ny"
      }
    },
    {
      "cell_type": "code",
      "source": [
        "#Dataset for the new version\n",
        "new_version = df[df['app_version'] == '1.2.2']\n",
        "print(new_version.shape)\n",
        "#Dataset for the old version\n",
        "old_version = df[df['app_version'] == '1.2.1']\n",
        "print(old_version.shape)"
      ],
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "qEjxUzrQnd7n",
        "outputId": "64f6431a-6705-484b-8964-5d74a38857e5"
      },
      "execution_count": 90,
      "outputs": [
        {
          "output_type": "stream",
          "name": "stdout",
          "text": [
            "(77500, 9)\n",
            "(77500, 9)\n"
          ]
        }
      ]
    },
    {
      "cell_type": "markdown",
      "source": [
        "Now that we have our data ready, let's apply what we learnt before. With the following function, we are going to:\n",
        "\n",
        "- **Calculate** the **mean** and **standard deviation** of each **variable** of each **dataset**.\n",
        "- **Check** if our **sample sizes** are **big enough** to guarantee a **statistical power** of at least **80%** in order to apply a **one-sided hypothesis test**.\n",
        "- **Apply** a **one-sided hypothesis test** to check if the **mean** that was the **biggest** is **significantly bigger** than the **mean** of the other **dataset** (app version).\n",
        "- **Calculate** the **mean percentage difference** between **app versions**.\n",
        "- **Calculate** the **confidence interval** of the **percentage difference**.\n",
        "\n",
        "\n",
        "\n",
        "\n"
      ],
      "metadata": {
        "id": "LjQV5N56oROw"
      }
    },
    {
      "cell_type": "code",
      "source": [
        "import numpy as np\n",
        "from scipy import stats\n",
        "import matplotlib.pyplot as plt\n",
        "from statsmodels.stats.power import TTestIndPower\n",
        "\n",
        "#Since we're working with two variants, let's assume a confidence level of 0.95\n",
        "def calculate_statistics_and_compare(df1, df2, confidence_level = 0.95, minimal_statistical_power = 0.8):\n",
        "  results = []\n",
        "  #Print table header with new columns\n",
        "  print(f\"{'Variable':<30}  {'Mean1':<10} {'Std1': <10} {'N1':<5} {'Mean2':<10} {'Std2': <10} {'N2':<5} {'P-Value': <10} {'% Difference':<15} {'% CI Lower Value':<20} {'% CI Upper Value':<20} {'Samples Big Enough?':<10}\")\n",
        "  #Table header separator\n",
        "  print(\"-\" * 200)\n",
        "  #For the numerical columns in the dataset:\n",
        "  for column in df1.columns:\n",
        "    if df1[column].dtype in ['float64', 'int64'] and column in df2.columns and column!= 'user_id':\n",
        "      mean1, mean2 = df1[column].mean(), df2[column].mean()\n",
        "      std1, std2 = df1[column].std(), df2[column].std()\n",
        "      n1, n2 = df1['user_id'].nunique(), df2['user_id'].nunique()\n",
        "\n",
        "      #Since we don't know which variant is going to be better (df1 or df2), let's do the following\n",
        "      #1- Perform a two-sided test to check if there is a significant difference between the means in either direction.\n",
        "      t_stat, p_value = stats.ttest_ind(df1[column], df2[column], equal_var = False)\n",
        "      #2- Since what we really want is a one-tailed test (testing whether the mean of one group is significantly greater than the mean of the other group),\n",
        "      #the p-value returned by ttest_ind should be converted to a one-tailed p-value. To achieve this, we divide it by 2.\n",
        "      #We divide it by two because the probability is distributed across both tails in a two-tailed test, but in a one-tailed test, you are only interested in one direction of the distribution.\n",
        "      one_sided_p_value = p_value / 2\n",
        "\n",
        "      # Check if the sample sizes are big enough to guarantee at least 80% statistical power\n",
        "      # Calculating Cohen's d\n",
        "      d = (mean2 - mean1) / np.sqrt((std1**2 + std2**2) / 2)\n",
        "      if mean2 > mean1:\n",
        "        alternative = 'larger'\n",
        "      elif mean2 < mean1:\n",
        "        alternative = 'smaller'\n",
        "      else:\n",
        "        if mean2 > 0:  # If mean2 is positive, consider 'larger' alternative\n",
        "            alternative = 'larger'\n",
        "        else:  # If mean2 is negative, consider 'smaller' alternative\n",
        "            alternative = 'smaller'\n",
        "      #Calculate the sample size required for ensuring the minimal statistical power\n",
        "      sample_size = TTestIndPower().solve_power(effect_size=d, alpha=1-confidence_level, power=minimal_statistical_power, alternative=alternative)\n",
        "\n",
        "      #Are the samples big enough?\n",
        "      if n1 >= sample_size and n2 >= sample_size:\n",
        "        samples_big_enough = 'big_enough'\n",
        "      else:\n",
        "        samples_big_enough = 'not_big_enough'\n",
        "\n",
        "      #Calculation of Standard Error and Confidence Interval\n",
        "      std_error = np.sqrt(std1**2/n1 + std2**2/n2)\n",
        "      z_score = stats.norm.ppf(confidence_level)\n",
        "      ci_range = z_score * std_error / mean1 * 100\n",
        "\n",
        "      #Calculate the mean percentage difference of mean2 (new version) with mean1 (old version)\n",
        "      mean_diff_percentage = ((mean2 -mean1)/mean1) * 100 if mean1 !=0 else np.nan\n",
        "\n",
        "      #Calculate the CI of the mean percentage difference of mean2 with mean1\n",
        "      #Calculate the lower bound of the CI\n",
        "      ci_lower_percentage = mean_diff_percentage - ci_range\n",
        "      #Calculate the upper bound of the CI\n",
        "      ci_upper_percentage = mean_diff_percentage + ci_range\n",
        "\n",
        "      #Print data\n",
        "      print(f\"{column:<30}  {mean1:<10.2f} {std1:<10.2f} {n1:<5} {mean2:<10.2f} {std2:<10.2f} {n2:<5} {round(one_sided_p_value, 4):<10} {round(mean_diff_percentage,3):<15} {round(ci_lower_percentage,3):<20} {round(ci_upper_percentage,3):<20} {samples_big_enough:<10} \")\n",
        "\n",
        "\n"
      ],
      "metadata": {
        "id": "mHsNJA-qrNRa"
      },
      "execution_count": 91,
      "outputs": []
    },
    {
      "cell_type": "code",
      "source": [
        "calculate_statistics_and_compare(old_version, new_version)"
      ],
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "WJ5Wjpg-K8Xh",
        "outputId": "0ffbc437-db47-42a7-f326-1dab4f240cad"
      },
      "execution_count": 92,
      "outputs": [
        {
          "output_type": "stream",
          "name": "stdout",
          "text": [
            "Variable                        Mean1      Std1       N1    Mean2      Std2       N2    P-Value    % Difference    % CI Lower Value     % CI Upper Value     Samples Big Enough?\n",
            "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n",
            "sessions_number                 4.49       2.29       2500  4.50       2.29       2500  0.1288     0.294           -2.081               2.668                not_big_enough \n",
            "minutes_spent_in_the_app        12.52      6.64       2500  12.47      6.65       2500  0.0644     -0.409          -2.878               2.059                not_big_enough \n",
            "produced_ad_revenue             0.05       0.03       2500  0.05       0.03       2500  0.4578     -0.03           -2.661               2.6                  not_big_enough \n",
            "produced_iap_revenue            5.50       2.61       2500  5.50       2.60       2500  0.3813     0.073           -2.128               2.273                not_big_enough \n"
          ]
        }
      ]
    },
    {
      "cell_type": "markdown",
      "source": [
        "In this case, we don't see **significant differences** between the old version of our app deployed at 80% and the new one deployed at 20% on Google Play.\n",
        "\n",
        "- The **percentage difference** of the values of the **numerical variables** of the new version with the ones of the old version are very small.\n",
        "- There is no **p-value** equal to or smaller than 0.05.\n",
        "- The **confidence intervals** include 0, indicating a non-significant difference.\n",
        "- And the **sample sizes** are not big enough to ensure a minimal **statistical power** of 80% that would detect a true effect.\n",
        "\n",
        "So, it would seem that our new version doesn't bring any improvements to the **KPIs** of the game. Better luck next time!\n",
        "\n"
      ],
      "metadata": {
        "id": "5VCKcc-SKZFj"
      }
    },
    {
      "cell_type": "markdown",
      "source": [
        "# 7. Conclusions\n",
        "\n",
        "\n",
        "A/B Testing can be messy, but I hope that this guide will help your process of finding interesting insights from your A/B test. Remember that it's always a good idea not only to use the data provided by the Firebase Console, but also the data exported from Firebase Google Analytics to BigQuery, as it will allow you to extract more useful information."
      ],
      "metadata": {
        "id": "0Iy-aTvfM2q0"
      }
    }
  ]
}